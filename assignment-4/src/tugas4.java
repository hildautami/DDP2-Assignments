import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JSplitPane;


public class tugas4 extends JFrame {
    private static final long serialVersionUID = 1L;
    private Board mBoard;
    // GUI components
    private JButton mRetryButton;
    private JButton mNewButton;
    private JSplitPane mSplitPane;

    public tugas4(){
        super();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBackground(Color.WHITE);

        mBoard = new Board();
        add(mBoard, BorderLayout.CENTER);

        mSplitPane = new JSplitPane();
        add(mSplitPane, BorderLayout.SOUTH);

        mRetryButton = new JButton("Retry");
        mRetryButton.setFocusPainted(false);
        mRetryButton.addMouseListener(btnMouseListener);
        mSplitPane.setLeftComponent(mRetryButton);

        mNewButton = new JButton("New Game");
        mNewButton.setFocusPainted(false);
        mNewButton.addMouseListener(btnMouseListener);
        mSplitPane.setRightComponent(mNewButton);

        pack();
        setResizable(true);
        setVisible(true);
    }

    private MouseListener btnMouseListener = new MouseAdapter() {
        public void mouseClicked(MouseEvent e) {
            if (e.getClickCount() == 1 && e.getComponent() == mRetryButton) {
                mBoard.reInit();
            } else if (e.getClickCount() == 1 && e.getComponent() == mNewButton) {
                mBoard.init();
            }
        }
    };

    public static void main(String[] args) {
        new tugas4();
    }
}
