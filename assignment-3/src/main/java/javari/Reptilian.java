package main.java.javari;

import main.java.javari.animal.Animal;
import main.java.javari.animal.Condition;
import main.java.javari.animal.Gender;

public class Reptilian extends Animal {
    /**
     * Constructs an instance of {@code Animal}.
     *
     * @param id        unique identifier
     * @param type      type of animal, e.g. Hamster, Cat, Lion, Parrot
     * @param name      name of animal, e.g. hamtaro, simba
     * @param gender    gender of animal (male/female)
     * @param length    length of animal in centimeters
     * @param weight    weight of animal in kilograms
     * @param condition health condition of the animal
     */

    private String specKondisi;

    public Reptilian(Integer id, String type, String name, Gender gender, double length, double weight, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
    }

    public Reptilian(Integer id, String type, String name, Gender gender, double length, double weight, String s, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.specKondisi = s;
    }

    @Override
    public boolean specificCondition() {
        //iya wild, jd gk bisa perform
        if (specKondisi.equals("wild")){
            return false;
        }

        //kondisi jinak, bisa perform
        return true;
    }
}
