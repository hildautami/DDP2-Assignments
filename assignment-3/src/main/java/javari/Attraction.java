package main.java.javari;

import main.java.javari.animal.Animal;
import main.java.javari.park.Registration;
import main.java.javari.park.SelectedAttraction;

import java.util.List;

public class Attraction implements SelectedAttraction {
    @Override
    public String getName() {
        return null;
    }

    @Override
    public String getType() {
        return null;
    }

    @Override
    public List<Animal> getPerformers() {
        return null;
    }

    @Override
    public boolean addPerformer(Animal performer) {

        return false;
    }
}
