package main.java.javari;

import main.java.javari.animal.Animal;
import main.java.javari.animal.Condition;
import main.java.javari.animal.Gender;

public class Mammals extends Animal {
    /**
     * Constructs an instance of {@code Animal}.
     *
     * @param id        unique identifier
     * @param type      type of animal, e.g. Hamster, Cat, Lion, Parrot
     * @param name      name of animal, e.g. hamtaro, simba
     * @param gender    gender of animal (male/female)
     * @param length    length of animal in centimeters
     * @param weight    weight of animal in kilograms
     * @param condition health condition of the animal
     */

    private String specKondisi;

    public Mammals(Integer id, String type, String name, Gender gender, double length, double weight, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
    }

    public Mammals(Integer id, String type, String name, Gender gender, double length, double weight, String s, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.specKondisi = s;
    }

    //iya kondisi pregnant : true
    @Override
    public boolean specificCondition() {
        //tidak hamil
        if (!specKondisi.equals("pregnant")){
            return false;
        }

        //lion gendernya cewek
        if (getType().equals("lion")){
            if (getGender().equals("female")){
                return false;
            }
        }

        //kalau hamil
        return true;
    }
}
