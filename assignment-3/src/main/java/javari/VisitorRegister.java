package main.java.javari;

import main.java.javari.park.Registration;
import main.java.javari.park.SelectedAttraction;

import java.util.ArrayList;
import java.util.List;

public class VisitorRegister implements Registration {
    List<SelectedAttraction> attractionList;
    String nama;
    int id;

    public VisitorRegister(int id) {
        this.id = id;
        this.attractionList = new ArrayList<>();
    }

    @Override
    public int getRegistrationId() {
        return this.id=this.id+1;
    }

    @Override
    public String getVisitorName() {
        return this.nama;
    }

    @Override
    public String setVisitorName(String name) {
        return this.nama=name;
    }

    @Override
    public List<SelectedAttraction> getSelectedAttractions() {
        return this.attractionList;
    }

    @Override
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        this.attractionList.add(selected);
        return false;
    }

}
