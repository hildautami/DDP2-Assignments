package main.java.javari;

import main.java.javari.animal.Animal;
import main.java.javari.animal.Condition;
import main.java.javari.animal.Gender;
import main.java.javari.park.Registration;
import main.java.javari.reader.ReadAnimal;
import main.java.javari.reader.ReadAttraction;
import main.java.javari.reader.ReadCategories;
import main.java.javari.reader.ReadSection;
import main.java.javari.writer.RegistrationWriter;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class JavariPark {
    static Animal animals;
    static ArrayList<Animal> dataHewan = new ArrayList<>();
    static ArrayList<String> diffKategoriHewan = null;
    static String menu = "section";
    static String tipeHewan = "";
    static Scanner sc = new Scanner(System.in);


    public static void main(String[] args) throws IOException {
        System.out.println("Welcome to Javari Park Festival - Registration Service!");
        System.out.println("Please provide the source data path: ");

        ReadSection section = new ReadSection(Paths.get("E:\\halo\\animals_categories.csv"));
        System.out.println("found _"+section.countValidRecords()+"_ valid animal section and "+"_"+section.countInvalidRecords()+"_ invalid animal records (indikatornya kek mana)");

        ReadAttraction attraction = new ReadAttraction(Paths.get("E:\\halo\\animals_attractions.csv"));
        System.out.println("found _"+attraction.countValidRecords()+"_ valid animal attraction and "+"_"+attraction.countInvalidRecords()+"_ invalid animal records (indikatornya kek mana)");

        ReadCategories categories = new ReadCategories(Paths.get("E:\\halo\\animals_categories.csv"));
        System.out.println("found _"+categories.countValidRecords()+"_ valid animal categories and "+"_"+categories.countInvalidRecords()+"_ invalid animal records (indikatornya kek mana)");

        ReadAnimal readAnimal = new ReadAnimal(Paths.get("E:\\halo\\animals_records.csv"));
        System.out.println("found _"+readAnimal.countValidRecords()+"_ valid animal records and "+"_"+readAnimal.countInvalidRecords()+"_ invalid animal records (indikatornya kek mana)");

        //sebelum nambahin harus di cek dulu si hewan ini ada gk di kategori kalau iya baru tambahin berdasarkan kategorinya yg csv itu
        for (int a=0; a<readAnimal.getLines().size();a++){
            String[] animal = readAnimal.getLines().get(a).split(",");
            for (int b=0; b<section.getLines().size();b++){
                //cek apakah animal masuk kategori yang terdata
                if (section.getLines().get(b).contains(animal[1])){
                    String[] sec = section.getLines().get(b).split(",");
                    Integer id = Integer.parseInt(animal[0]);
                    String type = animal[1];
                    String name = animal[2];
                    Gender gender = Gender.parseGender(animal[3]);
                    double length = Double.parseDouble(animal[4]);
                    double weight = Double.parseDouble(animal[5]);
                    Condition condition = Condition.parseCondition(animal[7]);

                    if (sec[1].equals("mammals")){
                        //System.out.println("wah ada mammals " +animal[1]+" "+animal[2]);
                        animals = new Mammals(id,type,name,gender,length,weight,animal[6],condition);
                        dataHewan.add(animals);
                    } else if (sec[1].equals("aves")){
                        //System.out.println("wah ada reptiles "+animal[1]);
                        animals = new Aves(id,type,name,gender,length,weight,condition);
                        dataHewan.add(animals);
                    } else if (sec[1].equals("reptiles")){
                        //System.out.println("wah ada aves "+animal[1]);
                        animals = new Reptilian(id,type,name,gender,length,weight,animal[6],condition);
                        dataHewan.add(animals);
                    } else {
                        System.out.println("kategori tidak ada");
                    }
                }
            }
        }

        System.out.println("Welcome to Javari Park Festival - Registration Service!\n Please answer the questions by typing the number. Type # if you want to return to the previous menu");
        Boolean status = true;

        while (true){
            switch (menu){
                case "section": //nampilin 4 section yang ada
                    //nampilin sectionnya aja.
                    int i=1;
                    for (String item: section.getDifList()) {
                        System.out.println(i+". "+item);
                        i++;
                    }
                    menu = "preferredSection";
                    break;


                case "preferredSection":  //nampilin semua hewan di section tertentu
                    System.out.print("Please choose your preferred section (type the number):");
                    Integer choosenSection = sc.nextInt();
                    switch (choosenSection){
                        case 1:
                            tipeHewan = "mammals";
                            ShowAnimal(choosenSection, tipeHewan);
                            menu = "preferredAnimals";
                            break;
                        case 2:
                            tipeHewan = "aves";
                            ShowAnimal(choosenSection, tipeHewan);
                            menu = "preferredAnimals";
                            break;
                        case 3:
                            tipeHewan = "reptil";
                            ShowAnimal(choosenSection, tipeHewan);
                            menu = "preferredAnimals";
                            break;
                    }
                    break;

                case "preferredAnimals":  //nampilin semua hewan di section tertentu
                    System.out.print("Please choose your preferred animal (type the number):");
                    Integer choosenAnimal = sc.nextInt();
                    ChoosenAnimal(choosenAnimal, attraction);
                    break;

                case "#":
                    menu = "preferredAnimals";
                    break;
            }
        }
    }



    private static void ChoosenAnimal(Integer choosenAnimal, ReadAttraction attraction) {
        boolean show = false;
        String dataHewanAtraksi = "";
        String chTypeAnimal = "";
        String atraksiAnimal = "";

        for (int i = 0; i < diffKategoriHewan.size(); i++) {
            if (diffKategoriHewan.get(i).equals(diffKategoriHewan.get(choosenAnimal - 1))) {
                chTypeAnimal = diffKategoriHewan.get(i);
            }
        }

        VisitorSelectedAttraction selectedAttraction = new VisitorSelectedAttraction();

        for (int i = 0; i < dataHewan.size(); i++) {
            if (dataHewan.get(i).getType().equals(chTypeAnimal)) {
                if (!dataHewan.get(i).specificCondition()) {
                    //animShow.add(dataMammals.get(i).getName());
                    dataHewanAtraksi = dataHewanAtraksi + dataHewan.get(i).getName() + "#";
                    selectedAttraction.addPerformer(dataHewan.get(i));
                    show = true;
                }
            }
        }

        if (show) {
            int j = 0;
            //tampilin atraksinya kalau memenuhi kondisi.
            for (String item : attraction.getLines()) {
                if (item.contains(chTypeAnimal)) {
                    atraksiAnimal = atraksiAnimal +item.split(",")[1] + "#";
                    System.out.println(j + 1 + ". " + item.split(",")[1]);
                    j++;
                }
            }

            System.out.print("Please choose your preferred atraction (type the number):");
            Integer choosenAtraction = sc.nextInt();
            String chAttraction = atraksiAnimal.split("#")[choosenAtraction-1];

            VisitorRegister register = new VisitorRegister(0);
            VisitoreResgistration(selectedAttraction,chAttraction,register,1);

       /*     System.out.print("register again ? : ");
            Scanner scanner = new Scanner(System.in);
            boolean y = true;
            int n=1;*/



            /*while (scanner.equals(y)){
                n++;
                register = new VisitorRegister(n);
                VisitoreResgistration(selectedAttraction,chAttraction,register, n);
            }*/

        }

    }

    private static void VisitoreResgistration(VisitorSelectedAttraction attraction, String atraksi,VisitorRegister reg,int u) {
        System.out.println("wooow one more step, \n please let us know your name : ");
        Scanner scanner = new Scanner(System.in);
        String namaVisitor = scanner.nextLine();
        reg.setVisitorName(namaVisitor);
        List<String> list = new ArrayList<>();

        List<Animal> data = attraction.getPerformers();
        for (int i=0; i<data.size();i++) {
            list.add(data.get(i).getName());
        }

        System.out.println("==============================================");
        System.out.println("yeay, final check!\nHere is your data, and the attraction you choose :");
        System.out.println("Name : "+reg.getRegistrationId());
        System.out.println("Name : "+reg.getVisitorName());
        System.out.println("Attraction : "+atraksi+" -> "+attraction.getType());
        System.out.println("with : "+String.join(",",list));

        reg.addSelectedAttraction(attraction);
        try {
            RegistrationWriter.writeJson(reg,Paths.get("E:\\halo\\"));
        } catch (IOException e) {
            e.printStackTrace();
        }



        /*System.out.print("Is the data correct ? (y/n) ");
        if (scanner.nextLine().equals('y')){
            reg.addSelectedAttraction(attraction);
            try {
                RegistrationWriter.writeJson(reg,Paths.get("C:///users/hilda/"));
                System.out.println("yeaah berhasil");
            } catch (IOException e) {
                System.out.println("kenpaa yaa");
                System.out.println(e.getMessage());
            }
        }*/

    }

    private static void cetakJson(Registration registration) {
        try {
            RegistrationWriter.writeJson(registration,Paths.get("C:///users/hilda/"));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    //chanimal = pilihan tipe hewan, cek dulu ada gk Hamster di list data seluruh data mammals (hamser,lion,cat)
    //attraction yang bisa ditambilkan kalau nilai spesific kondisinya true
    //specific kondisi nilainya true kalau pregnant dan lion male (mammals)
    //specific kondisi nilainya true kalau laying egg (aves)
    //specific kondisi nilainya true kalau tame (reptile)
    private static void ShowAnimal(Integer choosenSection, String pilihanHewan) {
        System.out.println("--Explores the --" + pilihanHewan);
        boolean show = false;
        String halo = "";
        diffKategoriHewan = new ArrayList<>();

        //ini buat nampilin ada hewan apa aja per kategori
        for (Animal item : dataHewan) {
            switch (choosenSection) {
                case 1:
                    if (item instanceof Mammals) {
                        if (diffKategoriHewan.size() == 0) {
                            diffKategoriHewan.add(item.getType());
                        } else {
                            if (!diffKategoriHewan.contains(item.getType())) {
                                diffKategoriHewan.add(item.getType());
                            }
                        }
                    }
                    break;

                case 2:
                    if (item instanceof Aves) {
                        if (diffKategoriHewan.size()==0) {
                            diffKategoriHewan.add(item.getType());
                        } else {
                            if (!diffKategoriHewan.contains(item.getType())) {
                                diffKategoriHewan.add(item.getType());
                            }
                        }
                    }
                    break;

                case 3:
                    if (item instanceof Reptilian) {
                        if (diffKategoriHewan.size()==0) {
                            diffKategoriHewan.add(item.getType());
                        } else {
                            if (!diffKategoriHewan.contains(item.getType())) {
                                diffKategoriHewan.add(item.getType());
                            }
                        }
                    }
                    break;
            }
        }

        for (String tipe: diffKategoriHewan) {
            System.out.println(tipe);
        }

    }

}
