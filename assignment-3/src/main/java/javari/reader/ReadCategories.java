package main.java.javari.reader;

import main.java.javari.reader.CsvReader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

public class ReadCategories extends CsvReader {
    ArrayList<String> difList = new ArrayList<>();
    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *                     properly
     */
    public ReadCategories(Path file) throws IOException {
        super(file);
    }

    @Override
    public long countValidRecords() {
        for (int i=0; i<getLines().size();i++){
            String tipe = getLines().get(i).split(",")[2];
            if (i==0){
                difList.add(tipe);
            } else {
                if (!difList.contains(tipe)){
                    difList.add(tipe);
                }
            }
        }
        return difList.size();
    }


    @Override
    public long countInvalidRecords() {
        return 0;
    }

    public ArrayList<String> getDifList() {
        return difList;
    }
}
