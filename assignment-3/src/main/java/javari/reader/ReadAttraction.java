package main.java.javari.reader;

import main.java.javari.reader.CsvReader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReadAttraction extends CsvReader {

    List<String> listAttraction = new ArrayList<>();
    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *                     properly
     */
    public ReadAttraction(Path file) throws IOException {
        super(file);
    }

    @Override
    public long countValidRecords() {
        ArrayList<String> difList = new ArrayList<>();
        for (int i=0; i<getLines().size();i++){
            String tipe = getLines().get(i).split(",")[1];
            if (i==0){
                difList.add(tipe);
            } else {
                if (!difList.contains(tipe)){
                    difList.add(tipe);
                }
            }
        }
        return difList.size();
    }

    @Override
    public long countInvalidRecords() {
        /*int c = 0;
        if (!getLines().contains("Circle of Fire") || !getLines().contains("Dancing Animals") || !getLines().contains("Counting Masters") || !getLines().contains("Passionate Coders:")){
            c+=1;
        }*/
        return 0;
    }
}
