package main.java.javari;

import main.java.javari.animal.Animal;
import main.java.javari.park.SelectedAttraction;

import java.util.ArrayList;
import java.util.List;

public class VisitorSelectedAttraction implements SelectedAttraction {
    List<Animal> listPerformer ;
    String type;
    String name;

    public VisitorSelectedAttraction() {
        this.listPerformer = new ArrayList<>();
        this.type = "";
        this.name = "";
    }

    @Override
    public String getName() {
        /*for (Animal animal: listPerformer) {
            this.name = this.name + "," +animal.getName();
        }*/
        return this.name;
    }

    @Override
    public String getType() {
        for (Animal animal: listPerformer) {
            this.type = animal.getType();
        }
        return this.type;
    }

    @Override
    public List<Animal> getPerformers() {
        return listPerformer;
    }

    @Override
    public boolean addPerformer(Animal performer) {
        listPerformer.add(performer);
        return true;
    }
}
