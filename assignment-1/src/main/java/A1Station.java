import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    // You can add new variables or methods in this class

    public static void main(String[] args) {

    	Scanner input = new Scanner(System.in);
        Scanner input2 = new Scanner(System.in);
    	Integer n = input.nextInt();
    	WildCat cat;
    	TrainCar car;
        double total=0.0;
        double totalMass=0.0;
        String tampil="";
        boolean startAgain = false;

        
        for (int i=1; i<=n; i++) {
    		String command = input2.nextLine();
			String[] splitCommand = command.split(",");

    		cat = new WildCat(splitCommand[0], Integer.parseInt(splitCommand[1]), Integer.parseInt(splitCommand[2]));
            car = new TrainCar(cat);
            //menambahkan refernce ke car untuk kucing kedua dstnya
            if (i==1 || i==n || startAgain==true) {
                car = new TrainCar(cat);
            } else {
                car = new TrainCar(cat, car);
            }
            //System.out.println(car.computeTotalMassIndex());

            total += car.computeTotalWeight();
            totalMass += car.computeTotalMassIndex();
            tampil += car.printCar();

            if (car.computeTotalWeight()>THRESHOLD) {
                System.out.println("The train departs to Javari Park");
                System.out.println("[LOCO] <" + tampil);
                System.out.println("Average mass index of all cats "+totalMass);
                System.out.println("In average, the cats in the train are "+ CatCategory(total, n));
                total = 0.0;
                totalMass = 0.0;
                startAgain = true;
                cat.setName("cobade");
            }
        }

        if (total>THRESHOLD) {
            System.out.println("The train departs to Javari Park");
            System.out.println("[LOCO] <" + tampil);
            System.out.println("Average mass index of all cats "+totalMass);
            System.out.println("In average, the cats in the train are "+ CatCategory(total, n));
            total = 0.0;
            totalMass = 0.0;
            startAgain = true;
        } else  {
            System.out.println("The train departs to Javari Park");
            System.out.println("[LOCO] <" + tampil);
            System.out.println("Average mass index of all cats "+totalMass);
            System.out.println("In average, the cats in the train are "+ CatCategory(total, n));
        }

       

   
        input2.close();

        input.close();

    }

    public static String CatCategory(double totalWeight, int totalCat){
        double averageWeight = totalWeight/totalCat;
        if (averageWeight>18.5) {
            return "underweight";
        } else if (averageWeight >= 18.5 && averageWeight<25.0) {
            return "normal";
        } else if (averageWeight >= 25.0 && averageWeight<30.0) {
            return "overweight";
        } else {
            return "obese";
        }

    }
    
}
