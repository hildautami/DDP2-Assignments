public class WildCat {

    // TODO Complete me!
    private String name;
    private double weight; // In kilograms
    private double length; // In centimeters

    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    public void setName(String nama){
        this.name = nama;
    }

    public String getName(){
        return "--" + " ("+name+")";
    }

    public void setWeight(double berat){
        this.weight = berat;
    }

    public double getWeight(){
        return weight;
    }

    public void setLength(double panjang){
        this.length = panjang;
    }

    public double getLength(){
        return length;
    }

    public double computeMassIndex() {

        double BMI = this.getWeight() / (this.getLength()/100*this.getLength()/100);
        // TODO Complete me!
        return (double)Math.round(BMI * 100.0)/100.0;
    }
}
