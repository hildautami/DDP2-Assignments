public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms
    WildCat cat;
    TrainCar next;

    // TODO Complete me!

    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

/*    public void setNext(TrainCar car){
        this.next = next;
    }
*/
    public double computeTotalWeight() {
        if(this.next==null) {
            return this.cat.getWeight();
        }else{
            return this.cat.getWeight()+this.next.computeTotalWeight();
       }
    }

    public double computeTotalMassIndex() {
        //double totalMassIndex = 0;
        if (this.next==null) {
            return this.cat.computeMassIndex();
        } else{
            return this.cat.computeMassIndex()+this.next.computeTotalMassIndex();
        }
    }

    public String printCar() {
        if (this.next==null) {
            return this.cat.getName();
        } else{
            return this.next.printCar();
        }
    }
}
